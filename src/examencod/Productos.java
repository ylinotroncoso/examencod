/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examencod;

/**
 *
 * @author ylinotroncoso
 */

public class Productos {
    /**
     * Atributos de nombre y precio
     */
       private String nombre;
       private float precio;
 
    /**
     * Constructor por defecto
     */
    public Productos(){
        nombre=null;
        precio=0.0F;
    }
    /**
     * 
     * @param nombre del producto
     * @param precio del producto
     */
     public Productos(String nombre,float precio){
        this.nombre=nombre;
        this.precio=precio;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the precio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    
}
